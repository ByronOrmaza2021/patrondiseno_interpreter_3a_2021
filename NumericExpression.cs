﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interpret
{
    public class NumericExpression : IExpression
    {
        private string Valor;

        public NumericExpression(string token)
        {
            //El token se usa para definir el número
            Valor = token;  
        }

        public void interpret(Context context)
        {
            context.setOperator(context.GetIntenger(Valor));   
            //Convierte un número escrito en letras en un entero
            context.calculate();   
            //Calcula el resultado
        }
    }
}

