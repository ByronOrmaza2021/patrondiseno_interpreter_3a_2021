﻿using Microsoft.VisualBasic.CompilerServices;
using System.Collections.Generic;
using System;
namespace Interpret
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] tree;
            var context = new Context();
            var expressions = new List<IExpression>();
            Console.WriteLine("SUMA y/o RESTA");
            Console.WriteLine("Digite la operación (en letras) a calcular: ");
            string Texto = Console.ReadLine();
            tree = Texto.Split(' '); //Separa nuestra operación ingresada
            IExpression Interpretacion;
            foreach (var T in tree)
            {
                if (Operators.ConditionalCompareObjectGreaterEqual(context.GetIntenger(T), 0, false))
                {
                    Interpretacion = new NumericExpression(T);
                }
                else
                {
                    Interpretacion = new OperatorExpression(T);
                }
                Interpretacion.interpret(context);
            }
            Console.WriteLine("El resultado para '" + Texto + "' es " + context.getResult());
            Console.ReadKey();
        }
    }
}

