﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interpret
{
    public interface IExpression
    {
        void interpret(Context context);
    }
    //Esta interfaz define una operación intérprete.
}

