﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interpret
{
    public class OperatorExpression : IExpression
    {
        private string operation;

        public OperatorExpression(string token)
        {
            //El "token" se usa para definir nuestro operador 
            operation = token;
        }

        public void interpret(Context context)
        {
            //Traduce o interpreta el operador matemático ingresado
            context.setOperation(operation);
        }
    }
}

