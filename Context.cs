﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Interpret
{
    public class Context
    {
        private string NextOperation = "";
        private int Operator = 0;
        private int Resultado = 0;
                
        public int GetIntenger(string input)
        {
            var switchExpr = input.ToLower();
            //Interpretación a número
            switch (switchExpr)
            {
                case "cero":  
                    { 
                        return 0;  
                    }
                case "uno":
                    {
                        return 1;
                    }
                case "dos":
                    {
                        return 2;
                    }
                case "tres":
                    {
                        return 3;
                    }
                case "cuatro":
                    {
                        return 4;
                    }
                case "cinco":
                    {
                        return 5;
                    }
                case "seis":
                    {
                        return 6;
                    }
                case "siete":
                    {
                        return 7;
                    }
                case "ocho":
                    {
                        return 8;
                    }
                case "nueve":
                    {
                        return 9;
                    }
                default:
                    {
                        return -1;
                    }
            }
        }

        public void setOperator(int Op)
        {
            Operator = Op;
        }

        public void setOperation(string operation)
        {
            //Traducción de la palabra mas a operador +
            if (operation.ToLower().Equals("mas"))
            {
                NextOperation = "+";
            }
            //Traducción de la palabra menos a operador -
            else if (operation.ToLower().Equals("menos"))
            {
                NextOperation = "-";
            }
        }

        public void calculate()
        {
            //Analiza cada simbolo y calcula la respuesta según el simbolo determinado
            if (NextOperation.Equals("+") || NextOperation.Equals(""))
            {
                Resultado += Operator;
            }
            else if (NextOperation.Equals("-"))
            {
                Resultado -= Operator;
            }
        }

        public int getResult()
        {
            return Resultado;
        }
    }
}

